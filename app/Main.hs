module Main (main) where

import Lib
import Web.Scotty
import Maze.Generator
import Maze.Types
import Data.Aeson

main :: IO ()
main = scotty 3000 $ do
    -- get "/:word" $ do
    --     beam <- param "word"
    --     html $ mconcat ["<h1>", "Hello, ", beam, "!", "<h1>"]

    get "/" $ do
        html $ mconcat ["<h1>Hello, world!</h1>"]

    get "/maze" $ do
        mazeAlgo <- param "algo"
        -- let mazeType = Maze.Types.toShape (param "type")
        mazeType <- param "type"
        mazeSize <- param "size"
        -- html $ "<p>" <>
        --         "Requested a " <> mazeType <> " maze with a size of "
        --         <> mazeSize <> " using the algorithm " <> mazeAlgo
        --         <> "</p>"
        let res = Maze.Generator.make mazeType mazeAlgo mazeSize
        case res of
            Right res -> Web.Scotty.json $ res
            Left res -> Web.Scotty.json $ object ["error" .= res]
