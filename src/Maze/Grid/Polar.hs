module Maze.Grid.Polar where

import Maze.Types
import Data.Graph.Inductive (Gr, edgeLabel, labEdges, nodes, order, prettyPrint)
import qualified Data.Graph.Inductive as Graph
import qualified Data.List as List
import qualified Data.Text as T


circumference :: Double -> Double
circumference radius = 2 * pi * radius

radius :: Int -> Int -> Double
radius level maxLevel = (fromIntegral level) / (fromIntegral maxLevel)

rowHeight :: Int -> Double
rowHeight level = 1 / (fromIntegral level)

cellWidth :: Double -> Int -> Double
cellWidth circ lvl = circ / (fromIntegral lvl)

-- Cell count by level
numCellsForLevel :: Int -> Int -> Int
numCellsForLevel _ 0 = 1
numCellsForLevel maxLevel level = if level <= maxLevel
                                  then previousCount * ratio
                                  else error("level > maxLevel")
  where previousCount = numCellsForLevel maxLevel (level - 1)
        ratio = round(estimatedCellWidth / rowHeight maxLevel)
        estimatedCellWidth =  cellWidth (circumference rad) previousCount
        rad = radius level maxLevel

cellsByLevel :: Int -> Int -> [Int]
cellsByLevel _ 0 = [0]
cellsByLevel maxLevel level = [x | x <- [1..numCells]]
  where numCells = numCellsForLevel maxLevel level


cells :: Int -> [Int]
cells 0 = [0]
cells level = innerCells ++ [x + offset | x <- [1..numCells]]
  where innerCells = cells (level - 1)
        offset = last innerCells
        numCells = numCellsForLevel level level

cellId :: Int -> Int -> Int -> Int
cellId _ 0 _ = 0
cellId maxLevel level index = offset + (index `mod` numCellsForCurrentLevel)
  -- if index < (numCellsForLevel maxLevel level)
  --                             then offset + index
  --                             else error("Index too big")
  where offset = sum $ map (numCellsForLevel maxLevel) [0..(level - 1)]
        numCellsForCurrentLevel = numCellsForLevel maxLevel level

cellRatio :: Int -> Int -> Int -> Double
cellRatio _ 0 _ = fromIntegral 1
cellRatio maxLevel outerLevel innerLevel = (fromIntegral numCellsOuterLevel) / (fromIntegral numCellsInnerLevel)
  where numCellsOuterLevel = length $ cellsByLevel maxLevel outerLevel
        numCellsInnerLevel = length $ cellsByLevel maxLevel innerLevel

-- cellLevel 4 43 = 4
-- cellLevel 4 56 = 4
-- cellLevel 4 43 = 3
-- cellLevel 4 18 = 2
-- cellLevel 4  5 = 1
-- cellLevel 4  0 = 0
cellLevel :: Int -> Int -> Int -> Int
cellLevel maxLevel currLevel idx = if currValue >= 0
                                   then cellLevel maxLevel nextLevel currValue
                                   else currLevel
  where cellsForLevel = numCellsForLevel maxLevel currLevel
        currValue = idx - cellsForLevel
        nextLevel = currLevel + 1


cellIndex :: Int -> Int -> Int -> Int
cellIndex maxLevel currLevel idx = if currValue >= 0
                                   then cellIndex maxLevel nextLevel currValue
                                   else idx
  where cellsForLevel = numCellsForLevel maxLevel currLevel
        currValue = idx - cellsForLevel
        nextLevel = currLevel + 1


data Coordinates = Coordinates {level :: Int, index :: Int} deriving Show

coordinates :: Int -> Int -> Coordinates
coordinates _ 0 = Coordinates {level = 0, index = 0}
coordinates maxLevel cId = Coordinates {level = cLevel, index = cIndex }
  where cLevel = cellLevel maxLevel 0 cId
        cIndex = cellIndex maxLevel 0 cId
    
inner :: Int -> Int -> Int -> Maybe [Int]
inner _ 0 _ = Nothing
inner maxLevel level index = if ratio == 1 || level - 1 == 0
                             then Just [(cellId maxLevel (level - 1) index)]
                             else
                                if index `mod` 2 == 0
                                then Just [(cellId maxLevel (level - 1) innerIndex)]
                                else Just [innerCw, innerCcw]
  where ratio = cellRatio maxLevel level (level - 1)
        innerIndex = round ((fromIntegral index) / ratio)
        innerCw = if (innerIndex * 2) < index
                  then cellId maxLevel (level - 1) (innerIndex)
                  else cellId maxLevel (level - 1) (innerIndex - 1)
        innerCcw = if (innerIndex * 2) < index
                   then cellId maxLevel (level - 1) (innerIndex + 1)
                   else cellId maxLevel (level - 1) (innerIndex)

outer :: Int -> Int -> Int -> Maybe [Int]
outer maxLevel 0 _ = Just (cellsByLevel maxLevel 1)
outer maxLevel level index = if (upperLevel > maxLevel || maxLevel <= level)
                             then Nothing
                             else
                              if ratio == 1
                              then Just [(cellId maxLevel upperLevel index)]
                              else Just [upperCw, facingCell, upperCcw]
  where upperLevel = level + 1
        ratio = cellRatio maxLevel upperLevel level
        facingCell = cellId maxLevel upperLevel $ index * (round ratio)
        upperCcw = cellId maxLevel upperLevel $ (index * round ratio) + 1
        upperCw = cellId maxLevel upperLevel $ (index * round ratio) - 1

ccw :: Int -> Int -> Int -> Maybe Int
ccw _ 0 _ = Nothing
ccw maxLevel level index = Just $ cellId maxLevel level (index + 1)

cw :: Int -> Int -> Int -> Maybe Int
cw _ 0 _ = Nothing
cw maxLevel level index = Just $ cellId maxLevel level (index - 1)


maybeToList :: Maybe [a] -> [a]
maybeToList Nothing = []
maybeToList (Just xs) = xs

neighbours :: Int -> Int -> [Int]
neighbours maxLevel cId = [x | (Just x) <- maybeNeighbours]
  where ccwCell = ccw maxLevel cLevel cIndex
        cwCell = cw  maxLevel cLevel cIndex
        innerCells = maybeToList (inner maxLevel cLevel cIndex)
        outerCells = maybeToList (outer maxLevel cLevel cIndex)
        coord = coordinates maxLevel cId
        cLevel = (level coord)
        cIndex = (index coord)
        maybeNeighbours = [cwCell, ccwCell] ++ [(Just x) | x <- innerCells] ++ [Just y | y <- outerCells]

toNode :: Int -> Graph.Node
toNode cId = (cId)

makeEdge :: Int -> Int -> Graph.LEdge ()
makeEdge cId neighId = ((toNode cId), (toNode neighId), ())

-- Grid
type Grid = Gr () ()

grid :: Int -> Grid
grid size = if size > 0
            then Graph.mkGraph nodes edges
            else error("Size must be greater than 0")

  where nodes = [(node, ()) | node <- nodeList]
        -- edges = []
        edges = [(makeEdge (fst n) n') | n <- nodes, n' <- (neighbours maxLevel (fst n))]
        nodeList = cells (size - 1)
        maxLevel = size - 1


showGrid :: Grid -> [T.Text]
showGrid grid = map (\x -> (T.pack (List.intercalate ";" (map show x)))) edgeList
  where edgeList = (fmap (\x -> x : Graph.suc grid x) (Graph.nodes grid))

-- makeGraph :: Int -> T.Text
-- makeGraph size = List.intercalate "\n" ((show size) :  showGrid (grid size))

-- writeGraph :: T.Text -> Int -> IO ()
-- writeGraph filePath size = do
--   writeFile filePath (makeGraph size)

