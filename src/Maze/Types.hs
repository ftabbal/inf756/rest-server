module Maze.Types where

import qualified Data.Text as T
import qualified Data.Text.Read as Reader
import Data.Aeson
import GHC.Generics
import Data.Either

data Size = Radius Int | WidthAndHeight Int Int deriving (Show, Generic)


type Connections = [T.Text]
data Shape = Circle | Rectangle deriving (Show, Enum)

type Algorithm = T.Text

data Graph = Graph {
    shape :: Shape
  , connections ::  Connections
  , size :: Size
} deriving (Show)

instance ToJSON Graph where
    toJSON (Graph aShape someConnections aSize) =
        object [ "shape" .= aShape
               , "connections" .= someConnections
               , "size" .= aSize
        ]

instance ToJSON Size where
    toJSON (Radius r) = object ["radius" .= r]
    toJSON (WidthAndHeight w h) = object ["w" .= w, "h" .= h]

instance ToJSON Shape where
    toJSON (Circle) = "Circle"
    toJSON (Rectangle) = "Rectangle"

-- instance ToJSON Size where
--     toJSON (Radius r) = r
--     toJSON (WidthAndHeight w h) = object ["w" .= w, "h" .= h]


toShape :: T.Text -> Either T.Text Shape
toShape res = if res == "Circle"
            then Right Circle
            else if res == "Rectangle"
            then Right Rectangle
            else Left (T.append "Unsupported shape: " res)

toSize :: T.Text -> Either T.Text Size
toSize query | numCommas == 0 = toRadius query
             | numCommas == 1 = toWidthAndHeight query
             | numCommas /= 0 && numCommas /= 1 = Left (T.append "Provided size is invalid: " query)
  where numCommas = T.count "," query 

toRadius :: T.Text -> Either T.Text Size
toRadius val =  case maybeSize of
                Right num -> 
                    if (fst num) < 3
                    then Left "Size must be greater than 3"
                    else if (fst num) > 15
                    then Left "Size must be less than 15"
                    else (Right $ Radius $ fst num)
                Left _ -> Left (T.append "Could not parse value: " val)

  where maybeSize = Reader.decimal val

toWidthAndHeight :: T.Text -> Either T.Text Size
toWidthAndHeight query = if (length parseResult == 2)
                         then Right (WidthAndHeight w h)
                         else Left (T.append "Invalid width and height from " query)
  where parseResult = Data.Either.rights (map (\x -> Reader.decimal x) (T.splitOn ","  query))
        w = fst (head parseResult)
        h = fst (last parseResult)


toAlgo :: T.Text -> Either T.Text Algorithm
toAlgo name = Right name