module Maze.Generator where

import Maze.Types
import qualified Maze.Grid.Polar as PolarGrid
import qualified Data.Graph.Inductive as Graph
import qualified Data.Text as T


make :: T.Text -> T.Text -> T.Text -> Either T.Text Graph
make aShape anAlgo aSize = graph <$> eitherShape <*> eitherAlgo <*> eitherSize
  where eitherShape = Maze.Types.toShape aShape
        eitherAlgo = Maze.Types.toAlgo anAlgo
        eitherSize = Maze.Types.toSize aSize

graph :: Shape -> Algorithm -> Size -> Graph
graph aShape anAlgo aSize = Maze.Types.Graph { shape = aShape
                                      , size = aSize
                                      , connections = someConnections}
  where someConnections = case aSize of
                            Maze.Types.Radius r  -> PolarGrid.showGrid $ PolarGrid.grid r
                            _ -> ["Connection 1", "Connection2 "]
-- make shape algo size = Maze